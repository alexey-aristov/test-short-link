﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ShortLink.Api;
using ShortLink.Services;
using ShortLink.Storage;

namespace ShortLink.App
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddShortLinkWebApi();
            services.AddShortLinkServices();
            services.AddShortLinkStore();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseShortLinkWebApi();
        }
    }
}
