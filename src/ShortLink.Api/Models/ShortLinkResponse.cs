﻿using System;

namespace ShortLink.Api.Models
{
    public class ShortLinkResponse
    {
        public Uri Short { get; set; }
        public Uri Original { get; set; }
    }
}
