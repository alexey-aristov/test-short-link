﻿using System;

namespace ShortLink.Api.Models
{
    public class CreateShortLinkRequest
    {
        public Uri Link { get; set; }
    }
}
