﻿namespace ShortLink.Api.Models
{
    public class ShortLinkCounterResponse
    {
        public ShortLinkResponse Link { get; set; }
        public int Count { get; set; }
    }
}
