﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace ShortLink.Api
{
    public static class SwaggerConfigureExtensions
    {
        public static void AddShortLinkSwagger(this IServiceCollection services)
        {
            services.AddSwaggerDocument(document =>
            {
                document.DocumentName = "ShortLink api";
                document.Title = "ShortLink api";
            });
        }

        public static void UseShortLinkSwagger(this IApplicationBuilder app)
        {
            app.UseOpenApi();
            app.UseSwaggerUi3();
        }
    }
}
