﻿using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using ShortLink.Auth;

namespace ShortLink.Api
{
    public static class WebApiConfigureExtensions
    {
        public static void AddShortLinkWebApi(
            this IServiceCollection services)
        {
            services.AddShortLinkSwagger();
            services.AddShortLinkAuth();
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddApplicationPart(Assembly.GetExecutingAssembly())
                .AddControllersAsServices();

        }

        public static void UseShortLinkWebApi(this IApplicationBuilder app)
        {
            app.UseShortLinkSwagger();
            app.UseShortLinkAuth();
            app.UseMvc();
        }
    }
}
