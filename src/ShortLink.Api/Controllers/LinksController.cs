﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ShortLink.Api.Models;
using ShortLink.Auth.Abstractions;
using ShortLink.Services.Abstractions;
using ShortLink.Storage.Abstractions;
using ShortLink.Storage.Models;

namespace ShortLink.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinksController : ControllerBase
    {
        private readonly IShortLinkGeneration shortLinkGeneration;
        private readonly ILinksStore linksStore;
        private readonly IUserStore userStore;

        public LinksController(
            IShortLinkGeneration shortLinkGeneration,
            ILinksStore linksStore,
            IUserStore userStore)
        {
            this.shortLinkGeneration = shortLinkGeneration;
            this.linksStore = linksStore;
            this.userStore = userStore;
        }

        [HttpGet]
        public async Task<IReadOnlyCollection<ShortLinkCounterResponse>> GetAll()
        {
            var user = userStore.GetUser();
            var links = await linksStore.GetAllLinks(user.Id);
            return links.Select(link => new ShortLinkCounterResponse()
            {
                Link = new ShortLinkResponse
                {
                    Short = link.Link.ShortLink,
                    Original = link.Link.OriginalLink
                },
                Count = link.Count
            }).ToList();
        }

        [HttpGet("decode/{shortLink}")]
        public async Task<Uri> GetOriginal([FromRoute]Uri shortLink)
        {
            // todo: uri isn't autodecoded, dont want to spend time for manual binding.
            var shortLinkStr = System.Net.WebUtility.UrlDecode(shortLink.OriginalString);
            var shortLinkDecoded = new Uri(shortLinkStr);

            var user = userStore.GetUser();
            var link = await linksStore.GetLinkByShort(shortLinkDecoded, user.Id);
            await linksStore.SaveLinkDecodeAttempt(link.Id, user.Id);
            return link.OriginalLink;
        }

        [HttpPost]
        public async Task<ShortLinkResponse> CreateShortLink(CreateShortLinkRequest createShortLink)
        {
            var user = userStore.GetUser();
            var generatedShortUri = shortLinkGeneration.CreateShortLink(createShortLink.Link);
            var link = new Link
            {
                OriginalLink = createShortLink.Link,
                ShortLink = generatedShortUri,
                UserId = user.Id
            };

            await linksStore.SaveLink(link);
            return new ShortLinkResponse
            {
                Short = link.ShortLink,
                Original = link.OriginalLink
            };
        }
    }
}
