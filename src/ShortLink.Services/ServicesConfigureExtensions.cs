﻿
using Microsoft.Extensions.DependencyInjection;
using ShortLink.Services.Abstractions;

namespace ShortLink.Services
{
    public static class ServicesConfigureExtensions
    {
        public static void AddShortLinkServices(this IServiceCollection services)
        {
            services.AddTransient<IShortLinkGeneration, GuidShortLinkGeneration>();
        }
    }
}
