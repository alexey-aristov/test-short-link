﻿using System;

namespace ShortLink.Services.Abstractions
{
    public interface IShortLinkGeneration
    {
        Uri CreateShortLink(Uri originalUri);
    }
}
