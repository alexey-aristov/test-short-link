﻿using ShortLink.Services.Abstractions;
using System;

namespace ShortLink.Services
{
    public class GuidShortLinkGeneration: IShortLinkGeneration
    {
        private readonly Uri baseUri = new Uri("http://short.uri");
        public Uri CreateShortLink(Uri originalUri)
        {
            var shortUriPart = Guid.NewGuid();
            return new Uri(baseUri,shortUriPart.ToString());
        }
    }
}
