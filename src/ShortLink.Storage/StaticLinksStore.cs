﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ShortLink.Storage.Abstractions;
using ShortLink.Storage.Models;

namespace ShortLink.Storage
{
    public class StaticLinksStore : ILinksStore
    {
        private static int id = 0;


        private static readonly ConcurrentDictionary<Guid, ConcurrentDictionary<Uri, Link>> userToShortToLinkMap
            = new ConcurrentDictionary<Guid, ConcurrentDictionary<Uri, Link>>();

        private static readonly ConcurrentDictionary<Guid, ConcurrentDictionary<int, List<LinkDecodeAttempt>>> decodeAttempts
            = new ConcurrentDictionary<Guid, ConcurrentDictionary<int, List<LinkDecodeAttempt>>>();
        public Task SaveLink(Link link)
        {
            Interlocked.Increment(ref id);
            link.Id = id;
            var links = userToShortToLinkMap.GetOrAdd(link.UserId, guid => new ConcurrentDictionary<Uri, Link>());
            links[link.ShortLink] = link;

            return Task.CompletedTask;
        }

        public Task SaveLinkDecodeAttempt(int linkId, Guid userId)
        {
            var dictionary = decodeAttempts.GetOrAdd(userId, guid => new ConcurrentDictionary<int, List<LinkDecodeAttempt>>());
            var list = dictionary.GetOrAdd(linkId, i => new List<LinkDecodeAttempt>());
            list.Add(new LinkDecodeAttempt
            {
                Date = DateTimeOffset.UtcNow,
                LinkId = linkId
            });
            return Task.CompletedTask;
        }

        public Task<Link> GetLinkByShort(Uri shortLink, Guid userId)
        {
            var link = userToShortToLinkMap[userId][shortLink];
            return Task.FromResult(link);
        }

        public Task<IReadOnlyCollection<LinkCounter>> GetAllLinks(Guid userId)
        {
            var attempts = decodeAttempts.GetOrAdd(userId, guid => new ConcurrentDictionary<int, List<LinkDecodeAttempt>>()); ;
            var links = userToShortToLinkMap[userId].Select(x => new LinkCounter
            {
                Link = x.Value,
                Count = attempts.TryGetValue(x.Value.Id, out var list) ? list.Count : 0
            }).ToList();
            return Task.FromResult((IReadOnlyCollection<LinkCounter>)links);
        }
    }
}