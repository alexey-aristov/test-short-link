﻿using System;

namespace ShortLink.Storage.Models
{
    public class LinkDecodeAttempt
    {
        public int LinkId { get; set; }
        public DateTimeOffset Date { get; set; }
    }
}
