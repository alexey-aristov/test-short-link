﻿using System;

namespace ShortLink.Storage.Models
{
    public class Link
    {
        public int Id { get; set; }
        public Uri ShortLink { get; set; }
        public Uri OriginalLink { get; set; }
        public Guid UserId { get; set; }
    }
}
