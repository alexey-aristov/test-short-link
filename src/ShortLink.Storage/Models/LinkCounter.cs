﻿namespace ShortLink.Storage.Models
{
    public class LinkCounter
    {
        public Link Link { get; set; }
        public int Count { get; set; }
    }
}
