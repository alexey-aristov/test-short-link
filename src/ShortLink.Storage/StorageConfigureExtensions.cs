﻿using Microsoft.Extensions.DependencyInjection;
using ShortLink.Storage.Abstractions;

namespace ShortLink.Storage
{
    public static class AuthConfigureExtensions
    {
        public static void AddShortLinkStore(this IServiceCollection services)
        {
            services.AddTransient<ILinksStore, StaticLinksStore>();
        }
    }
}
