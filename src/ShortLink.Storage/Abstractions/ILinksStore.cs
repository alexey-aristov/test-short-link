﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ShortLink.Storage.Models;

namespace ShortLink.Storage.Abstractions
{
    public interface ILinksStore
    {
        Task SaveLink(Link link);
        Task SaveLinkDecodeAttempt(int linkId, Guid userId);
        Task<Link> GetLinkByShort(Uri shortLink, Guid userId);
        Task<IReadOnlyCollection<LinkCounter>> GetAllLinks(Guid userId);
    }
}
