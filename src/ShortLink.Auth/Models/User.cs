﻿using System;
namespace ShortLink.Auth.Models
{
    public class User
    {
        public User(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
