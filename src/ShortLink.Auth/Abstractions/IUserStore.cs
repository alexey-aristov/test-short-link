﻿using ShortLink.Auth.Models;

namespace ShortLink.Auth.Abstractions
{
    public interface IUserStore
    {
        User GetUser();
    }
}
