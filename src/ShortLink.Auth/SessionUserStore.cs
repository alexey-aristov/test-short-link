﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using ShortLink.Auth.Abstractions;
using ShortLink.Auth.Models;

namespace ShortLink.Auth
{
    class SessionUserStore : IUserStore
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public SessionUserStore(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }
        public User GetUser()
        {
            var userId = GetOrSetUserId();
            return new User(Guid.Parse(userId));
        }

        private string GetOrSetUserId()
        {
            string userIdKey = "userId";
            var session = httpContextAccessor.HttpContext.Session;
            var userIdStr = session.GetString(userIdKey);

            if (string.IsNullOrWhiteSpace(userIdStr))
            {
                userIdStr = Guid.NewGuid().ToString();
                session.SetString(userIdKey, userIdStr);
            }


            return userIdStr;
        }
    }
}