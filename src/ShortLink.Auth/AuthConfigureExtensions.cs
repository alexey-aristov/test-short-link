﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using ShortLink.Auth.Abstractions;

namespace ShortLink.Auth
{
    public static class AuthConfigureExtensions
    {
        public static void AddShortLinkAuth(this IServiceCollection services)
        {
            services.AddScoped<IUserStore, SessionUserStore>();
            services.AddHttpContextAccessor();
            services.AddSession();
        }

        public static void UseShortLinkAuth(this IApplicationBuilder app)
        {
            app.UseSession();
        }
    }
}
