using ShortLink.Services;
using System;
using Xunit;

namespace ShortLink.Tests
{
    public class GuidShortLinkGenerationTests
    {
        [Fact]
        public void CanGenerate()
        {
            // Arrange
            var generation = new GuidShortLinkGeneration();
            var originalUri = new Uri("http://test.test/test/test/test");

            // Act
            var shortLink = generation.CreateShortLink(originalUri);

            // Assert
            Assert.Equal(2, shortLink.Segments.Length); // 1st segement - '/', 2nd - our short link part
        }
    }
}
